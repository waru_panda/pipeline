import random
from typing import Dict

from jsonschema import validate, ValidationError

from pipeline import Step
from pipeline.exceptions import StepException


class StepNames:
    SCHEMA_VALIDATION = 'schema_validation'
    DATA_VALIDATION = 'data_validation'
    ADDING_DIGIT = 'adding_digit'
    CALCULATE_VALUES = 'calculate_values'


# General steps
class ValidateSchemaStep(Step):
    name = StepNames.SCHEMA_VALIDATION

    schema = {
        'type': 'object',
        'properties': {}
    }

    def validate_schema(self, data: Dict) -> Dict:
        try:
            validate(data, schema=self.schema)
        except ValidationError as e:
            raise StepException('Invalid json schema')
        return data

    def run_step(self, data: Dict) -> Dict:
        return self.validate_schema(data)


class ValidateDataStep(Step):
    name = StepNames.DATA_VALIDATION

    @staticmethod
    def fake_validate_data(data: Dict) -> Dict:
        random_number = random.randint(0, 10)
        if random_number % 3 == 0:
            raise StepException('Fake validation failed')
        return data

    def run_step(self, data):
        return self.fake_validate_data(data)


# Service A
class ValidateAServiceSchemaStep(ValidateSchemaStep):
    schema = {
        'type': 'object',
        'properties': {
            'digits': {
                'type': ['array', 'null']
            },
            'max': {
                'type': ['number', 'null']
            },
            'min': {
                'type': ['number', 'null']
            },
            'avg': {
                'type': ['number', 'null']
            },
        }
    }


class AddDigitStep(Step):
    name = StepNames.ADDING_DIGIT

    @staticmethod
    def add_digit(data):
        new_number = random.randint(0, 100)
        digits = data.get('digits', [])
        digits.append(new_number)
        data['digits'] = digits
        return data

    def run_step(self, data):
        return self.add_digit(data)


# Service B
class ValidateBServiceSchemaStep(ValidateSchemaStep):
    schema = {
        'type': 'object',
        'properties': {
            'digits': {
                'type': ['array']
            },
            'max': {
                'type': ['number', 'null']
            },
            'min': {
                'type': ['number', 'null']
            },
            'avg': {
                'type': ['number', 'null']
            },
        }
    }


class CalculateValues(Step):
    name = StepNames.CALCULATE_VALUES

    @staticmethod
    def add_digit(data):
        digits = data['digits']
        data['max'] = max(digits)
        data['min'] = min(digits)
        data['avg'] = round(sum(digits) / len(digits), 2)
        return data

    def run_step(self, data):
        return self.add_digit(data)
