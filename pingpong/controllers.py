from flask import Blueprint, request, jsonify, abort, Response

from pingpong.service import AServiceHandler, BServiceHandler
from pipeline.exceptions import PipelineException

api = Blueprint('api', __name__)

SUCCESS_RESPONSE = {'status': 'ok'}


@api.route('/add_digit', methods=['POST'])
def a_service_pipeline():
    try:
        AServiceHandler(request.json).handle()
    except PipelineException as err:
        abort(Response(err.as_json(), 400))
    return jsonify(SUCCESS_RESPONSE)


@api.route('/update_values', methods=['POST'])
def b_service_pipeline():
    try:
        BServiceHandler(request.json).handle()
    except PipelineException as err:
        abort(Response(err.as_json(), 400))
    return jsonify(SUCCESS_RESPONSE)
