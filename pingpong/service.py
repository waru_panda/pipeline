from typing import Dict

import requests

from config import A_SERVICE_DESTINATION, B_SERVICE_DESTINATION
from pingpong.pipeline_steps import ValidateDataStep, AddDigitStep, CalculateValues, ValidateAServiceSchemaStep, \
    ValidateBServiceSchemaStep
from pipeline import Pipeline


class PipelineServiceHandler:
    result_destination = ''
    pipeline_steps = []

    def __init__(self, data: Dict):
        self.data = data
        self.pipeline = Pipeline()

    def handle(self):
        for step in self.pipeline_steps:
            self.pipeline.add_step(step)
        data = self.pipeline.run(self.data)
        requests.post(self.result_destination, json=data)


class AServiceHandler(PipelineServiceHandler):
    result_destination = B_SERVICE_DESTINATION
    pipeline_steps = [
        ValidateAServiceSchemaStep(),
        ValidateDataStep(),
        AddDigitStep(),
    ]


class BServiceHandler(PipelineServiceHandler):
    result_destination = A_SERVICE_DESTINATION
    pipeline_steps = [
        ValidateBServiceSchemaStep(),
        ValidateDataStep(),
        CalculateValues(),
    ]
