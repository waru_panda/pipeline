import abc
from typing import Dict

import log
from pipeline.exceptions import StepException, PipelineException


class Step(abc.ABC):
    @property
    @abc.abstractmethod
    def name(self):
        raise NotImplementedError

    @abc.abstractmethod
    def run_step(self, data: Dict) -> Dict:
        raise NotImplementedError


class Pipeline:
    def __init__(self):
        self.steps = []

    def add_step(self, step):
        self.steps.append(step)

    def run(self, data: dict):
        for step in self.steps:
            log.info(f'Running step "{step.name}"...')
            log.debug(f'Data for step "{step.name}": {data}')

            try:
                data = step.run_step(data)
            except StepException as err:
                log.error(f'Error on step "{step.name}": {err.message}')
                raise PipelineException(step.name, err.message)

            log.info(f'Step "{step.name}" successfully completed.')
        return data
