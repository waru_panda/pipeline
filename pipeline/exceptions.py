import json


class BasePipelineException(Exception):
    def __init__(self, message=None):
        self.message = message


class StepException(BasePipelineException):
    pass


class PipelineException(BasePipelineException):
    def __init__(self, step_name, message=None):
        self.step_name = step_name
        super(PipelineException, self).__init__(message)

    def as_json(self):
        return json.dumps({
            'step': self.step_name,
            'message': self.message
        })
