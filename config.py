HOST = '0.0.0.0'
PORT = 9000

A_SERVICE_DESTINATION = f'http://{HOST}:{PORT}/add_digit'
B_SERVICE_DESTINATION = f'http://{HOST}:{PORT}/update_values'
