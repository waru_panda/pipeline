from flask import Flask
from flask_appconfig import AppConfig

from config import HOST, PORT
from pingpong.controllers import api


def create_app(configfile=None):
    app = Flask(__name__)
    AppConfig(app, configfile=configfile)
    app.register_blueprint(api)
    return app


app = create_app("config.py")

if __name__ == '__main__':
    app.run(host=HOST, port=PORT, debug=False)
